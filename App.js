import { logicalExpression } from '@babel/types';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { Dimensions, View } from 'react-native';
import Dashboard from './src/Screens/Dashboard/Dashboard';
import Login from './src/Screens/Dashboard/Login';


const width=Dimensions.get('window').width;
const height=Dimensions.get('window').height;
const Stack=createStackNavigator();
export default class App extends React.Component{
  render(){
    return(
      <View style={{flex:1}}>
        <Login/>

        {/* <NavigationContainer>
          <Stack.Navigator headerMode={false}>
            <Stack.Screen name='Dashboard' component={Dashboard}/>

          </Stack.Navigator>
        </NavigationContainer> */}
      </View>
    )
  }
}