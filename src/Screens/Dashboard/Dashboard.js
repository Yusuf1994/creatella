import React, { useEffect, useState } from "react";
import { Alert, Dimensions, FlatList, SafeAreaView, Text, TouchableOpacity, View } from "react-native";
import { getEmoji } from "../../Utility/APICall";
import { getAdsData, getPrice, getTime, log } from "../../Utility/CommonFunctions";
import { NO_INTERNET } from "../../Utility/Constants";
const sorting = {

    size: 'size',
    id: 'id',
    price: 'price'
}
const { width, height } = Dimensions.get('window');
let bgColor = "#545454"
export default class Dashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            page: 1,
            isLoading: true,
            sortingParam: sorting.id,
        }
    }

    componentDidMount() {
        this.hitApi();

    }

    hitApi() {

        getEmoji(this.state.page, this.state.sortingParam, (response) => {
            if (response.length > 0) {
                if (this.state.page == 1) {
                    let _data = getAdsData(response);
                    this.setState({ page: this.state.page + 1, data: _data })

                } else
                {
                    let filteredData= this.state.data.filter((item,index)=>{
                        return !item.isAds;
                    })
                    let _data = getAdsData( [...filteredData, ...response]);
                    this.setState({ page: this.state.page + 1, data:_data })

                }
                   

                    
            } else {
                this.setState({ isLoading: false })
            }


        }, (err) => {
            if (err?.message == NO_INTERNET) {
                alert(NO_INTERNET);
            }

        })
    }
    _renderItem = ({ item, index }) => {

        if(item.isAds){
return (
    <View style={{ flex:1, padding: 10, }}>
            <View style={{ justifyContent: "center", alignItems: "center", borderRadius: 10, backgroundColor: "red", flex: 1, padding: 10 }}>
                <Text style={{ fontSize: 20,color:"white" }}>
                    {"Ads Here"}
                </Text>
              
            </View>


        </View>
)


        }else
        return (<View style={{ width: width / 2, padding: 10, }}>
            <View style={{ justifyContent: "center", alignItems: "center", borderRadius: 10, backgroundColor: "white", flex: 1, padding: 10 }}>
                <Text style={{ fontSize: item.size }}>
                    {item.face}
                </Text>
                <Text style={{ fontSize: 14, marginTop: 10 }}>
                    {getPrice(item.price)}
                </Text>

                <Text style={{ fontSize: 12, marginTop: 10 }}>
                    {getTime(item.date)}
                </Text>
            </View>


        </View>)
    }

    _renderFooter = () => {
        return (<View style={{ alignItems: "center" }}>
            <Text style={{ fontSize: 14, color: "white" }}>
                {this.state.isLoading ? "loading..." : "~ end of catalogue ~"}
            </Text>
        </View>)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, width: '100%', height: '100%', backgroundColor: bgColor }}>
                <View style={{ width: '100%', height: 80, backgroundColor: bgColor }}>
                    <Text style={{ marginLeft: 10, color: 'white', fontSize: 15 }} > {'Sort By'}  </Text>
                    <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={{ backgroundColor: this.state.sortingParam == sorting.id ? 'gray' : 'white', width: '30%', height: 40, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }}
                            onPress={() => {
                                this.setState({ page: 1, data: [], sortingParam: sorting.id }, () => {
                                    this.hitApi();
                                })

                            }}
                        >
                            <Text style={{ color: this.state.sortingParam == sorting.id ? "white" : 'black' }}> {'Id'} </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ backgroundColor: this.state.sortingParam == sorting.size ? 'gray' : 'white', width: '30%', height: 40, justifyContent: 'center', alignItems: 'center' }}
                            onPress={() => {
                                this.setState({ page: 1, data: [], sortingParam: sorting.size }, () => {
                                    this.hitApi();
                                })

                            }}
                        >
                            <Text style={{ color: this.state.sortingParam == sorting.size ? "white" : 'black' }}> {'Size'} </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ backgroundColor: this.state.sortingParam == sorting.price ? 'gray' : 'white', width: '30%', height: 40, justifyContent: 'center', alignItems: 'center', borderBottomRightRadius: 5, borderTopRightRadius: 5 }}
                            onPress={() => {
                                this.setState({ page: 1, data: [], sortingParam: sorting.price }, () => {
                                    this.hitApi();
                                })
                            }}
                        >
                            <Text style={{ color: this.state.sortingParam == sorting.price ? "white" : 'black' }}> {'Price'} </Text>
                        </TouchableOpacity>
                    </View>


                </View>
                <FlatList
                    numColumns={2}
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => item.id}
                    ListFooterComponent={this._renderFooter}
                    onEndReachedThreshold={0.5}
                    onEndReached={() => { this.hitApi() }}
                />
            </SafeAreaView>
        );
    }

}