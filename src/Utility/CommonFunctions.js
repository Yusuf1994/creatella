import NetInfo from "@react-native-community/netinfo";
import moment from "moment";

export async function getNetInfo() {
    return NetInfo.fetch().then(state => {
        return state.isConnected;
    });

}

export function log(msg){
    console.log(msg)
}

export function getTime(time){
  return  moment(time).fromNow()
}

export function getPrice(cents){
    return  "$"+(cents/100).toFixed(2)
  }
  export function getAdsData(liveData,alternate=20){
    if(liveData.length>0){
    const ads = [{ id: "1" }]; // array of ads
    const content = liveData; // array of content
    if (content.length < alternate) {
    liveData = [...content, { ...ads[0], isAds: true }];
    } else {
    liveData = content.reduce((acc, curr, i) => {
    if ((i + 1) % alternate === 0) {
    const adIndex = Math.floor(i / alternate) % ads.length;
    return [...acc, curr, { ...ads[adIndex], isAds: true }];
    }
    
    return [...acc, curr];
    }, []);
    }
    return liveData;
    }else{
    return liveData;
    }
    }