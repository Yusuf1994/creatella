import { getNetInfo, log } from "./CommonFunctions";
import { API_DOMAIN, NO_INTERNET } from "./Constants";

async function apiRequest(url, successCallback, errorCallback) {

    log(url)
    let isConnected = await getNetInfo();

    if (isConnected) {

        let response = await fetch(url);
        let responseJson = await response.json();
        successCallback(responseJson);


    } else {
        errorCallback({ message: NO_INTERNET })
    }
}

export async function getEmoji(page,sortingParam,success,error) {
    return apiRequest(`${API_DOMAIN}/api/products?_page=${page}&_limit=20&_sort=${sortingParam}`,success,error)
}